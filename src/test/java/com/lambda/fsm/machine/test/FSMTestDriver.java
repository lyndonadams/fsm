package com.lambda.fsm.machine.test;

import com.lambda.fsm.machine.IMachine;
import com.lambda.fsm.machine.StateMachineFactory;
import com.lambda.fsm.rules.Predicate;
import com.lambda.fsm.rules.Rule;
import com.lambda.fsm.rules.RuleEvent;
import com.lambda.fsm.rules.RuleEventListener;
import com.lambda.fsm.state.State;
import com.lambda.fsm.state.StateEvent;
import com.lambda.fsm.state.StateEventListener;
import com.lambda.fsm.tasks.ITask;
import com.lambda.fsm.tasks.TaskEvent;
import com.lambda.fsm.tasks.TaskEventListener;
import com.lambda.fsm.tasks.impl.test.SimpleCompletedTask;
import com.lambda.fsm.tasks.impl.test.SimpleFailedTask;
import com.lambda.fsm.transition.Transition;

import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class FSMTestDriver {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		final File dic = FileUtils.getFile("src","test", "resources", "testxml5.xml");


		testXMLLoad( dic.getAbsolutePath() );
		
		//testTaskListeners();
		
		//ruleTaskInvokationTest();
		//ruleTester();
		//stateTester();	
	}
	
	
	public static void stateTester(){

		ITask[] tasks= {  new SimpleCompletedTask("a"), new SimpleFailedTask("b"),  new SimpleCompletedTask("c"), new SimpleFailedTask("d")	, new SimpleCompletedTask("e"), new SimpleFailedTask("f") };
		
		Rule r1 = new Rule("r1", tasks[0], tasks[1], Predicate.OR);
		Rule r2 = new Rule("r2", tasks[2], tasks[3], Predicate.OR);
		Rule r3 = new Rule( "r3", tasks[4], tasks[5], Predicate.OR);
				
		r1.addRule( r2, Predicate.OR );
		r2.addRule( r3, Predicate.OR );

		// Create a com.lambda.fsm.transition with com.lambda.fsm.rules
		Transition t = new Transition(r1);
		
		// States
		State state1 = new State("State 1");
		State state2 = new State("State 2");
		State state3 = new State("State 3");
		
		state1.addTransitionMap(t, state2);
		state2.addTransitionMap(t, state3);
		
		state1.addStateChangedListener( new StateEventListener() {

			public void stateFailed(StateEvent e) {
				System.out.println( "State 1 failed");
			}

			public void stateSuccessful(StateEvent e) {
				System.out.println( "State 1 success.");
			}
		});
		
		state2.addStateChangedListener( new StateEventListener() {

			public void stateFailed(StateEvent e) {
				System.out.println( "State 2 failed");
			}

			public void stateSuccessful(StateEvent e) {
				System.out.println( "State 2 success.");
			}
		});
		
		Thread td = new Thread( state1 );
		td.start();	
	}
	
	public static void testTaskListeners(){
	
		SimpleCompletedTask t = new SimpleCompletedTask("test");	
		t.addTaskListener( new TaskEventListener(){
			public void taskCompletedSuccessfully(TaskEvent event) {
				System.out.println( "taskCompletedSuccessfully: "  + event.getStatus().toString() );				
			}

			public void taskErrorOccurred(TaskEvent event) {
				System.out.println( event.getTaskException().toString() );
				
			}

			public void taskRunning(TaskEvent event) {
				System.out.println("Task: " + event.getStatus().toString() );
			}
			
		});
		
		SimpleFailedTask t2 = new SimpleFailedTask("test");	
		t2.addTaskListener( new TaskEventListener(){
			public void taskCompletedSuccessfully(TaskEvent event) {
				System.out.println( "taskCompletedSuccessfully: "  + event.getStatus().toString() );				
			}

			public void taskErrorOccurred(TaskEvent event) {
				System.out.println( event.getTaskException().toString() );
				
			}

			public void taskRunning(TaskEvent event) {
				System.out.println("Task: " + event.getStatus().toString() );
			}
			
		});
		
		Thread th1= new Thread( t2 );
		th1.start();		
		Thread th = new Thread( t );
		th.start();
		
		
	}
	
	
	public static void testXMLLoad(String filename) throws ParserConfigurationException, SAXException, IOException, Exception{
		
		DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
		factory.setValidating(false );
		DocumentBuilder builder = factory.newDocumentBuilder();
		builder.setErrorHandler(new ErrorHandler() {

			public void error(SAXParseException exception) throws SAXException {
				System.out.println("XML error");				
			}

			public void fatalError(SAXParseException exception) throws SAXException {
				System.out.println("XML fatalError");
			}

			public void warning(SAXParseException exception) throws SAXException {
				System.out.println("XML warning");
			}			
		});
		
		
		IMachine m = StateMachineFactory.createStateMachine( (Document) builder.parse(filename) );
		m.start();
	}
	
	
	public static void ruleTaskInvokationTest(){
		System.out.println("Task Tester");System.out.println("-----------");
		
		State state = new State();
		
		SimpleCompletedTask s1 = new SimpleCompletedTask("a");
		SimpleCompletedTask s2 = new SimpleCompletedTask("b");
		SimpleCompletedTask s3 = new SimpleCompletedTask("c");
		
		state.addTaskListener( s1 );
		state.addTaskListener( s2 );
		state.addTaskListener( s3 );

		
		Thread t1 = new Thread( s1 );
		Thread t2 = new Thread( s2 );
		Thread t3 = new Thread( s3 );
		
		t1.start();
		t2.start();
		t3.start();
			
	}
	
	public static void ruleTester(){
		
		System.out.println("Rule Tester");System.out.println("-----------");
		
		ITask[] tasks= {  new SimpleCompletedTask("a"), new SimpleCompletedTask("b"),  new SimpleFailedTask("c"), new SimpleFailedTask("d")	, new SimpleCompletedTask("e"), new SimpleFailedTask("f") };
		
		
		Rule r1 = new Rule("r1", tasks[0], tasks[1], Predicate.OR);
		//Rule r2 = new Rule("r2", com.lambda.fsm.tasks[2], com.lambda.fsm.tasks[3], Predicate.OR);
		//Rule r3 = new Rule( "r3", com.lambda.fsm.tasks[4], com.lambda.fsm.tasks[5], Predicate.OR);
				
//		r1.addRule( r2, Predicate.OR );
//		r2.addRule( r3, Predicate.OR );
		
		final long s = System.currentTimeMillis();
		
		r1.addRuleListener(  new RuleEventListener(){
						
			public void ruleFailed(RuleEvent e) {
				System.out.println("Rule Failed." + e.getRule().toString() );		
				System.out.println("Processing time took: " + (System.currentTimeMillis() - s));
			}

			public void rulePassed(RuleEvent e) {
				System.out.println("Rule Passed.");	
				System.out.println("Processing time took: " + (System.currentTimeMillis() - s));
			}
		});

		
		r1.startTasks();
	}

}
