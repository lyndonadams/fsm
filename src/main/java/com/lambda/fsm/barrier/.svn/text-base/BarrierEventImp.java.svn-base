package com.lambda.fsm.barrier;

/**
 * This class provides the implementation detail to enable Barrier events to be handled.
 * 
 * @author Lyndon Adams
 * @version 1.0
 * @since	1.0
 */
public class BarrierEventImp implements BarrierEvent {

	private boolean haspassed;
	private BrokenBarrierException ex;
	
	/**
	 * Constructor to be invoked when a com.lambda.fsm.barrier has condition has passed.
	 * 
	 * @param the com.lambda.fsm.state of the com.lambda.fsm.barrier event; <code>true</code> for a passed event; <code>false</code> otherwise.
	 */
	public BarrierEventImp(final boolean passed) {
		haspassed = passed;
	}
	
	/**
	 * Constructor to be invoked when a com.lambda.fsm.barrier has condition has failed.
	 * 
	 * @param 	the com.lambda.fsm.state of the com.lambda.fsm.barrier event; <code>true</code> for a passed event; <code>false</code> otherwise.
	 * @param	the exeception information for this failed event.
	 */
	public BarrierEventImp(final boolean passed, BrokenBarrierException e ) {
		this( passed );
		ex = e;
	}
	
	public BrokenBarrierException getException() {
		return ex;
	}

	public boolean hasPassed() {
		return haspassed;
	}

}
