package com.lambda.fsm.barrier;

/**
 * A semantic com.lambda.fsm.barrier event used to invoke and passed to a com.lambda.fsm.barrier for addition processing.
 *  
 * @author Lyndon Adams
 * @version 1.0
 * @since	1.0
 */
public interface BarrierEvent {
	
	/**
	 * Use to determine if com.lambda.fsm.barrier has completed.
	 *
	 * @return 		<code>true</code> if com.lambda.fsm.barrier event completed successfully; 
     *              <code>false</code> otherwise.
	 */
	boolean hasPassed();
	
	/**
	 * Use to determine if com.lambda.fsm.barrier has completed.
	 *
	 * @return 		<code>exception</code> representing the com.lambda.fsm.barrier failure com.lambda.fsm.state; 
	 */
	BrokenBarrierException getException();
}
