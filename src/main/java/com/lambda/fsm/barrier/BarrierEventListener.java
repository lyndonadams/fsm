package com.lambda.fsm.barrier;

/**
 * The listener interface for receiving com.lambda.fsm.barrier events. The class that is interested in processing an action event 
 * implements this interface, and the object created with that class is registered with a component, 
 * using the component's addBarrierListener method. When the action event occurs, that object's barrierPassed or barrierFailed
 * method is invoked depending upon com.lambda.fsm.state. 
 * 
 * @author Lyndon Adams
 * @version 1.0
 * @since	1.0
 */
public interface BarrierEventListener {
	
	/**
	 * Called only if the com.lambda.fsm.barrier has passed.
	 * @param e the com.lambda.fsm.barrier event to process.
	 */
	void barrierPassed(BarrierEvent e);
	
	/**
	 * Called only if the com.lambda.fsm.barrier has failed.
	 * @param e the com.lambda.fsm.barrier event to process.
	 */
	void barrierFailed(BarrierEvent e);
}
