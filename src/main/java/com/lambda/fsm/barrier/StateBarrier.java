package com.lambda.fsm.barrier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.lambda.fsm.state.IState;
import com.lambda.fsm.transition.ITransition;
import com.lambda.fsm.transition.TransitionEvent;
import com.lambda.fsm.transition.TransitionEventListener;

/**
 * 
 * @author Lyndon Adams
 * @version 1.0
 * @since	1.0
 */
public class StateBarrier implements Barrier {

	/*
	 * Logfile setting
	 */
	private Logger logger = Logger.getLogger( StateBarrier.class.getName() );
	
	private IState state;
	
	/**
	 * Internal status variables.
	 */
	private AtomicInteger nTansitions  = new AtomicInteger(0);
	private AtomicInteger nTansitionsPassed = new AtomicInteger(0);
	private AtomicBoolean isComplete = new AtomicBoolean( false);
	private AtomicBoolean hasFailed  = new AtomicBoolean( false);
	
	//private BarrierEvent be;
	
	// Threading variables
	private Lock barrierlock = new ReentrantLock();
	//private Condition barrierCondition = barrierlock.newCondition();
	
	protected List<BarrierEventListener> observers = Collections.synchronizedList( new ArrayList<BarrierEventListener>());

	/**
	 * Constructs a StateBarrier with the passed com.lambda.fsm.state to protect.
	 * 
	 * @param s the com.lambda.fsm.state to protect.
	 */
	public StateBarrier(IState s){
		state = s;
	}
	
	
	/**
	 * Add com.lambda.fsm.transition listener for com.lambda.fsm.barrier to listen too. Barrier assumes all transitions must pass 
	 * before a com.lambda.fsm.barrier passed signal is fired.
	 * 
	 * @param t the com.lambda.fsm.transition to listen too.
	 */
	public void addTransitionListener(ITransition t){		
		
		// This is wrong. Need to check if com.lambda.fsm.transition has been assigned before.
		nTansitions.incrementAndGet();
		
		
		t.addTransitionListener(new TransitionEventListener(){
			public void transitionRulesFailed(TransitionEvent e) {
				try {
					barrierlock.lock();
					isComplete.set( true);
					hasFailed.set( true );
					fireBarrierEvent( new BarrierEventImp( false, new BrokenBarrierException("Barrier broken." ) ));
				} finally {
					barrierlock.unlock();
				}
			}

			// THIS LOGIC IS NOT CORRECT
			public void transitionRulesSuccessful(TransitionEvent e) {
				try {
					barrierlock.lock();
					nTansitionsPassed.incrementAndGet();
					
					if( nTansitionsPassed.intValue() == nTansitions.intValue() ) {
						isComplete.set( true);
						hasFailed.set( false );	
						fireBarrierEvent(new BarrierEventImp(true ) );
					}
				} finally {
					barrierlock.unlock();
				}				
			}
		});
	}

	/**
	 * Gets the State object that the com.lambda.fsm.barrier protects.
	 * 
	 * @return The protected State object.
	 */
	public IState getProtectedState(){ return state; }
	
	public void fireBarrierEvent(BarrierEvent e) {
		// Fire event to listeners
		for(BarrierEventListener l: observers ){
			if( e.hasPassed() ) 
				l.barrierPassed( e); 
			else
				l.barrierFailed( e);
		}	
	}
	
	public void addBarrierListener(BarrierEventListener l) {
		observers.add( l);
	}


	public boolean isComplete() {
		return isComplete.get();
	}
	


	@Override
	public boolean failed() {
		return hasFailed.get();
	}
	
}
