package com.lambda.fsm.barrier;

/**
 * This com.lambda.fsm.barrier interface is used to determine if an incoming com.lambda.fsm.state can proceed. The com.lambda.fsm.barrier will be linked to only one com.lambda.fsm.state.
 * External listeners can subscribe to events from the com.lambda.fsm.barrier for feed back purposes.
 *  
 * @author Lyndon Adams
 * @version 1.0
 * @since	1.0
 */
public interface Barrier {
	
	/**
	 * Add a {@link BarrierEventListener} event listener to the com.lambda.fsm.barrier. 
	 * @param l is the listener to subscribe to the com.lambda.fsm.barrier.
	 */
	void addBarrierListener(BarrierEventListener l);
	
	/**
	 * Fire a com.lambda.fsm.barrier event from a {@link BarrierEvent} object.
	 * @param	evt the event to fire from.
	 */
	void fireBarrierEvent(BarrierEvent evt);
	
	/**
	 * Use to determine if com.lambda.fsm.barrier has completed.
	 *
	 * @return 		<code>true</code> if com.lambda.fsm.barrier completed; 
     *              <code>false</code> otherwise.
	 */
	boolean isComplete();
	
	/**
	 * Call to determine if com.lambda.fsm.barrier has failed.
	 *          
	 * @return 		<code>true</code> if com.lambda.fsm.barrier condition failed; 
     *              <code>false</code> otherwise com.lambda.fsm.barrier completed successfully.
	 */
	boolean failed();
}
