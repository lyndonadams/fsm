package com.lambda.fsm.barrier;

/**
 *  This exception is thrown by the StateBarrier to indicate that the com.lambda.fsm.state condition failed.
 *  
 * @author Lyndon Adams
 * @version 1.0
 * @since	1.0
 */
public class BrokenBarrierException extends Exception {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Default construtor
	 */
	private BrokenBarrierException(){ super(); }
	
	/**
	 * Constructs a BrokenBarrierException with the specified, detailed message. 
	 * 
	 * @param m the detail message
	 */
	public BrokenBarrierException(String m){ super(m); }
	
	/**
	 * Constructs a BrokenBarrierException with the specified detailed message and linked exception.
	 * 
	 * @param m the detail message
	 * @param e the linked exception.
	 */
	public BrokenBarrierException(String m, Exception e){ 
		super(m,e); 
	}
}
