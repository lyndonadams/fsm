package com.lambda.fsm.transition;

/**
 * 
 * @author Lyndon Adams
 *
 */
public interface TransitionEventListener {
	void transitionRulesSuccessful(TransitionEvent e);
	void transitionRulesFailed(TransitionEvent e);
}
