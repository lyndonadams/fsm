package com.lambda.fsm.transition;

/**
 * 
 * @author Lyndon Adams
 *
 */
public class TransitionEventImpl implements  TransitionEvent {
	
	private ITransition tr;
	private TransitionException ex;
	
	public TransitionEventImpl(ITransition t){
		tr = t;
	}

	public TransitionEventImpl(ITransition t, TransitionException e){
		this(t);
		ex =e;		
	}
	
	public ITransition getTransition() {
		return tr;
	}

	public TransitionException getTransitionException() {
		return ex;
	}

}
