package com.lambda.fsm.transition;

/**
 * 
 * @author Lyndon Adams
 *
 */
public interface TransitionEvent {
	public TransitionException getTransitionException();	
	public ITransition getTransition();
}
