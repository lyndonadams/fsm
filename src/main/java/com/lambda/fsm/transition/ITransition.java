package com.lambda.fsm.transition;

import com.lambda.fsm.rules.IRule;

/**
 * 
 * @author Lyndon Adams
 *
 */
public interface ITransition {

	/**
	 * Add rule to com.lambda.fsm.transition com.lambda.fsm.state
	 * @param r
	 */
	void setRule(IRule r);
	
	/**
	 * Remove rule from com.lambda.fsm.transition com.lambda.fsm.state.
	 * @param r
	 */
	IRule getRule();
	
	
	void addTransitionListener(TransitionEventListener e);
}
