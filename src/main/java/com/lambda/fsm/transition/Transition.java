package com.lambda.fsm.transition;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.lambda.fsm.rules.IRule;
import com.lambda.fsm.rules.RuleEvent;
import com.lambda.fsm.rules.RuleEventListener;

/**
 * 
 * @author Lyndon Adams
 *
 */
public class Transition implements ITransition {

	List<TransitionEventListener> observers = Collections.synchronizedList( new  ArrayList<TransitionEventListener>());

	// Single composite rule
	private IRule rule;
	
	// Threading variables
	private Lock transitionlock = new ReentrantLock();
	private Condition transitionCondition = transitionlock.newCondition();
	
	private AtomicBoolean hasfired  = new AtomicBoolean( false);
	

	/**
	 * Default constructor
	 *
	 */
	public Transition(){}
	
	/**
	 * Constructor with passed rule.
	 * @param r
	 */
	public Transition(IRule r){
		setRule( r);
	}
		
	/**
	 * Fire task events to listeners.
	 * @param e
	 */
	protected void fireTransitionEvent(RuleEvent e){
		for(TransitionEventListener l: observers ){			
			switch(  e.getStatus() ){
				case SUCCESS :	l.transitionRulesSuccessful( new TransitionEventImpl( this )); 
								break;
				case FAILED :	l.transitionRulesFailed( new TransitionEventImpl( this, new TransitionException("Tranistion com.lambda.fsm.rules failed" ) ));
								break;
				case UNKNOWN : 
				default  :	break;
			}
		}
	}
	
	/**
	 * Set com.lambda.fsm.transition rule.
	 */
	public void setRule(IRule r) {
		rule = r;
		rule.addRuleListener( new RuleEventListener(){
			
			public void ruleFailed(RuleEvent e) {
				if ( !hasfired.get() ){
					hasfired.set( true );
					fireTransitionEvent(e);	
				}
			}

			public void rulePassed(RuleEvent e) {
				if ( !hasfired.get() ){
					hasfired.set( true );
					fireTransitionEvent(e);	
				}
			}
		});
	}

	/**
	 * Get com.lambda.fsm.transition composite rule.
	 */
	public IRule getRule(){
		return rule;
	}

	/**
	 * Add com.lambda.fsm.transition event listener.
	 */
	public void addTransitionListener(TransitionEventListener e) {
		observers.add(e);		
	}

}
