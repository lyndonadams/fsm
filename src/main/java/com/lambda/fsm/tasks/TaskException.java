package com.lambda.fsm.tasks;

/**
 * 
 * @author Lyndon Adams
 *
 */
public class TaskException extends Exception {

	String errMsg;
	int exitCode;
	
	public TaskException(String e, int c){
		errMsg = e;
		exitCode = c;
	}
	
	public int getExitCode(){
		return exitCode;
	}

	public String toString(){
		return errMsg;
	}
	
}
