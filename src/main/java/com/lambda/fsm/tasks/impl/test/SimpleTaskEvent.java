package com.lambda.fsm.tasks.impl.test;

import com.lambda.fsm.tasks.ITask;
import com.lambda.fsm.tasks.TaskEvent;
import com.lambda.fsm.tasks.TaskException;
import com.lambda.fsm.tasks.TaskStatus;

/**
 * 
 * @author Lyndon Adams
 *
 */
public class SimpleTaskEvent  implements TaskEvent {

	ITask task;
	TaskException ex;
	
	public SimpleTaskEvent(ITask t){
		task = t;
	}
	
	public SimpleTaskEvent(ITask t, TaskException e){
		this(t);
		ex = e;			
	}

	public ITask getTask() {
			return task;
	}

	public TaskException getTaskException() {
		return ex;
	}

	public TaskStatus getStatus() {
		return task.getStatus();
	}
}
