package com.lambda.fsm.tasks.impl.test;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.lambda.fsm.tasks.ATask;
import com.lambda.fsm.tasks.TaskException;
import com.lambda.fsm.tasks.TaskStatus;


/**
 * 
 * @author Lyndon Adams
 *
 */
public class SimpleFailedTask extends ATask  {

	/*
	 * Logfile setting
	 */
	private Logger logger = Logger.getLogger( SimpleFailedTask.class.getName() );
	
	private long timeout = 2000;
	
	private Lock tasklock = new ReentrantLock();
	private Condition barrierCondition = tasklock.newCondition();
	
	public SimpleFailedTask(String n){
		name =n;
	}

	
	public SimpleFailedTask(String n, long t){
		this(n);
		timeout = t;
	}
	
	public String toString(){
		return name;
	}

	public void run() {
		try {
			tasklock.lock();
			//logger.log( Level.INFO,"Running " + name + " task");
			status.set( TaskStatus.RUNNING);
			Thread.sleep(timeout );
			status.set( TaskStatus.FAILED);
			fireTaskEvent( new SimpleTaskEvent(this, new TaskException("Task " + name + " has failed. Time: " + System.currentTimeMillis() , -1) ) );		
		} catch (InterruptedException e) {
			status.set( TaskStatus.FAILED);
			e.printStackTrace();
		} finally{
			tasklock.unlock();
		}
	}

}
