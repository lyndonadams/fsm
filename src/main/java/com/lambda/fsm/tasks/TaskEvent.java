package com.lambda.fsm.tasks;

/**
 * 
 * @author Lyndon Adams
 *
 */
public interface TaskEvent {
	public TaskStatus getStatus();
	public ITask getTask();
	public TaskException getTaskException();
}
