package com.lambda.fsm.tasks;

/**
 * 
 * @author Lyndon Adams
 *
 */
public enum TaskStatus {
	WAITING, RUNNING, COMPLETED, FAILED;
}
