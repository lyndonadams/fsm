package com.lambda.fsm.tasks;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 
 * @author Lyndon Adams
 *
 */
public abstract class ATask implements ITask {
	
	protected ArrayList<TaskEventListener> observers = new ArrayList<TaskEventListener>();
	protected AtomicReference<TaskStatus> status = new AtomicReference<TaskStatus>( TaskStatus.WAITING);
	
	protected String name;
	
	/**
	 * Add task listener.
	 */
	public void addTaskListener(TaskEventListener o){
		observers.add( o);
	}
	
	/**
	 * Get array of task listeners.
	 */
	public TaskEventListener[] getTaskListeners(){
		return (TaskEventListener[])observers.toArray();
	}

	/**
	 * Fire task events to listeners.
	 * @param e
	 */
	protected void fireTaskEvent(TaskEvent e){
		for(TaskEventListener l: observers ){
			switch(  e.getStatus() ){
				case COMPLETED : l.taskCompletedSuccessfully(e); break;
				case FAILED :	 l.taskErrorOccurred(e); break;
				case RUNNING :	 l.taskRunning(e); break;
				default	:		System.exit(-1 ); break;
			}
		}
	}
	
	/**
	 * 
	 */
	public TaskStatus getStatus() {
		return status.get();
	}
	
	public boolean isSuccess(){
		return (status.get().compareTo( TaskStatus.COMPLETED ) == 0) ? true : false;
	}
}
