package com.lambda.fsm.tasks;

/**
 * 
 * @author Lyndon Adams
 *
 */
public interface ITask extends Runnable {
	
	void addTaskListener(TaskEventListener e);
	TaskEventListener[] getTaskListeners();
	
	TaskStatus getStatus();
	
	boolean isSuccess();
	
}
