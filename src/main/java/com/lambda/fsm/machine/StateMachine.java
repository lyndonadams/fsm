package com.lambda.fsm.machine;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.lambda.fsm.executionpool.ExecutionThreadPool;
import com.lambda.fsm.rules.*;
import com.lambda.fsm.state.*;
import com.lambda.fsm.tasks.ITask;
import com.lambda.fsm.transition.*;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is the concrete implementation of the IMachine interface.
 *  
 * @author Lyndon Adams
 * @version 1.0
 * @since	1.0
 */
public class StateMachine implements IMachine {
		
	/*
	 * Logfile setting
	 */
	private Logger logger = Logger.getLogger( StateMachine.class.getName() );
	

	// Map of states
	private HashMap<String, IState>  states;
	private String startState;
	
	// Map of failed states
	private HashMap<String, IState>  failedstates = new HashMap<String, IState>();
	
	// Threading control flags
	private Lock statemachinelock = new ReentrantLock();
	private Condition statemachineCondition = statemachinelock.newCondition();
	
	/**
	 * Default construtor creating a empty com.lambda.fsm.state com.lambda.fsm.machine.
	 */
	public StateMachine(){}
	
	
	public void setupMachine(Document d) throws Exception {
		parseXMLDocument( d );
	}
	
	public void start() {
        logger.log(Level.INFO,"Machine start");
		ExecutionThreadPool.getInstance().execute( states.get( startState ) );
	}
	
	public IState[] getExecutingStates() {
		Set<String> keys = states.keySet();
		int i = 0;
		IState[] s = new IState[ keys.size() ];
		for(String key: keys ){
			if( states.get(key).getStatus().compareTo( StateStatus.RUNNING ) == 0 )
				s[i++] = states.get(key);	
		}
		return s;
	}
	
	/**
	 * Set up call back for passed com.lambda.fsm.state.
	 * 
	 * @param s the State to set up the call back.
	 */
	private void setupStateCallBack(IState s){
		s.addStateChangedListener( new StateEventListener() {

			public void stateFailed(StateEvent e) {
				try { 
					statemachinelock.lock();
					logger.log( Level.SEVERE, "State " + e.getState().toString() + " failed.");				
					failedstates.put( e.getState().toString(), e.getState() );
				} finally {
					statemachinelock.unlock();
				}
			}

			public  void stateSuccessful(StateEvent e) {
				
				try { 
					statemachinelock.lock();
					logger.log( Level.INFO, " State " + e.getState().toString() + " passed.");
									
					/**
					 * Is this the end com.lambda.fsm.state??
					 */
					if ( e.getState().getStatus().compareTo( StateStatus.COMPLETED )==0 && e.getState().isFinalState() ){
				
						logger.log( Level.INFO, " FINAL STATE ");
						
						// Check if there are any failed states
						if( failedstates.size() != 0 ){
							logger.log(Level.INFO, "WOULD YOU LIKE TO RERUN??");
						} else {
							
							logger.log( Level.INFO, "Shutting down execution pool." );
							List<Runnable> outstandingTasks = ExecutionThreadPool.getInstance().getPool().shutdownNow();
							logger.log( Level.INFO, "Machine completed successfully." + ( (outstandingTasks!=null)? "Outstanding com.lambda.fsm.tasks: " + outstandingTasks.size() : ""));
						}
					}	
				} finally {
					statemachinelock.unlock();
				}
				
			}
		});		
	}
	
	/**
	 * Parse xml dom to com.lambda.fsm.state com.lambda.fsm.machine.
	 * 
	 * @param d the com.lambda.fsm.state com.lambda.fsm.machine configuration xml document.
	 */
	private void parseXMLDocument(Document d) throws Exception  {	
		//	 Automata
		NodeList ns = d.getDocumentElement().getChildNodes();		
		
		// Go through every automata
		for( int i=0; i< ns.getLength(); i++){
			
			Node node_i = ns.item(i);
			if (node_i.getNodeType() == Node.ELEMENT_NODE  && ((Element) node_i).getTagName().equals("Automata")) {
			
				Element automata = (Element) node_i;
				
				// Create the states
				states = createStates( automata.getElementsByTagName( "State"));
				
				// Generate all com.lambda.fsm.tasks
				HashMap<String, ITask>  tasks = createTasks( automata.getElementsByTagName("Task") );
				
				//	 Generate all com.lambda.fsm.rules with linked com.lambda.fsm.tasks
				HashMap<String, IRule>  rules = createRules( automata.getElementsByTagName("Rule"), tasks );

				// Create chain com.lambda.fsm.rules is required.
				linkChainRules(automata.getElementsByTagName( "ChainedRule"), rules);

				//	 Apply transitions to states
				linkStatesAndTransition(automata.getElementsByTagName( "Transition"), states, rules );
			}	
		}		
	}
	

	/**
	 * Link states, transitions and com.lambda.fsm.rules together to build a com.lambda.fsm.state com.lambda.fsm.machine.
	 * 
	 * @param transitions the set of transitions.
	 * @param states the set of states
	 * @param com.lambda.fsm.rules the set of com.lambda.fsm.rules
	 * 
	 * @exception throws an exception if consistency checks fail.
	 */
	private void linkStatesAndTransition(NodeList transitions, HashMap<String, IState>  states, HashMap<String, IRule>  rules) throws Exception {
		for(int i=0; i < transitions.getLength(); i++){
			Element e = (Element)transitions.item(i);
			
			Transition t = new Transition();
			
			// Add the rule to the com.lambda.fsm.transition
			if( e.getAttribute("rule") != null  && rules.containsKey(e.getAttribute("rule") )) {
				t.setRule( rules.get(e.getAttribute("rule")) );
			}
			
			String src = e.getAttribute("src"); String trg = e.getAttribute("dst");
			
			if( src == null || trg == null ) throw new Exception("Unknown src or trg for com.lambda.fsm.transition. Check com.lambda.fsm.transition descriptions.");
			if( !states.containsKey( src ) || !states.containsKey( trg )) throw new Exception("Unknown src or trg com.lambda.fsm.state for com.lambda.fsm.transition. src: " + src + " trg: " + trg);
			
			// Now join the transitions to states.			
			states.get( src ).addTransitionMap(t, states.get( trg));
			
			// Add target com.lambda.fsm.state com.lambda.fsm.barrier
			states.get( trg ).addIncomingBarrierListener(t);
		}
	}

	
	/**
	 * Create a set of task objects ready to mapped later in the process.
	 * 
	 * @param nl list of com.lambda.fsm.tasks
	 * @return map of com.lambda.fsm.state name to com.lambda.fsm.tasks
	 * 
	 * STARTED TO LOOK AT THIS TO BE ABLE TO CREATE ANY FORM OF TASK
	 * NEED THE FULL ERROR HANDLING HERE
	 */
	private HashMap<String,ITask> createTasks(NodeList nl){
		HashMap<String, ITask> map = new HashMap<String, ITask>();
		for(int i=0; i < nl.getLength(); i++){
			Element e = (Element)nl.item(i);
			String name = e.getAttribute("name");
			//map.put( name, new SimpleCompletedTask(name));
			
			// Build and instantiate user class
			try {
				Class ctype = Class.forName( e.getAttribute("class") );
				Constructor ctor = ctype.getDeclaredConstructor(String.class);
				ITask t = (ITask) ctor.newInstance( name);
				
				map.put( name, t);
				
			} catch (ClassNotFoundException e1) {
				logger.log(Level.SEVERE, "Unknown task class. Please check classpath has correct task jar file included.");
				e1.printStackTrace();
			} catch (InstantiationException e1) {
				logger.log(Level.SEVERE,"Cannot create task object");
				e1.printStackTrace();
			} catch (IllegalAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NoSuchMethodException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalArgumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InvocationTargetException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		return map;
	}
	
	/**
	 * Create a set of states
	 * 
	 * @param nl the set of states to create.
	 * @return map of com.lambda.fsm.state names to com.lambda.fsm.state objects.
	 */
	private HashMap<String, IState> createStates(NodeList nl)throws Exception{
		HashMap<String, IState> map = new HashMap<String, IState>();
		for(int i=0; i < nl.getLength(); i++){
			Element e = (Element)nl.item(i);
			String name = e.getAttribute("name");
			
			boolean f =false;
			if( e.getAttribute("type").compareToIgnoreCase("START") == 0  && startState!=null ) throw new Exception("Found duplicate start states.");
			else if (  e.getAttribute("type").compareToIgnoreCase("START") == 0 ) {
				startState = name;
				f = true;
			}
			
			State state = new State( name, Integer.valueOf( e.getAttribute("seq")).intValue(), f );
			setupStateCallBack( state );
			map.put( name, state );
		}
		return map;
	}
	
	
	/**
	 * Create a set of com.lambda.fsm.rules mapped to com.lambda.fsm.tasks.
	 * 
	 * @param nl the set of com.lambda.fsm.rules
	 * @param com.lambda.fsm.tasks the hashmap set of com.lambda.fsm.tasks.
	 * 
	 * @return map of rule to com.lambda.fsm.tasks.
	 */
	private HashMap<String, IRule> createRules(NodeList nl, HashMap<String, ITask> tasks) throws Exception {
		HashMap<String, IRule> map = new HashMap<String, IRule>();
		
		for(int i=0; i < nl.getLength(); i++){
			Element e = (Element)nl.item(i);
			
			String name = e.getAttribute("name");
			if( !tasks.containsKey( e.getAttribute("task1")) || !tasks.containsKey( e.getAttribute("task2")) ) throw new Exception("Missing task for rule: " + name);
			map.put( name, new Rule(name, tasks.get(  e.getAttribute("task1")), tasks.get(  e.getAttribute("task2")), Predicate.valueOf( e.getAttribute("operator"))));
		}
		return map;
	}
	
	/**
	 * Link chain com.lambda.fsm.rules together.
	 * 
	 * @param nl list of source com.lambda.fsm.rules.
	 * @param ruleSet set of com.lambda.fsm.rules.
	 */
	private void linkChainRules(NodeList nl, HashMap<String, IRule> ruleSet) throws Exception {
		for(int i=0; i < nl.getLength(); i++){
			Element e = (Element)nl.item(i);
			
			String src = e.getAttribute("src");
			String crule = e.getAttribute("rule");
			
			if( ruleSet.containsKey( src )) throw new Exception("Unknown source rule: " + src);
			if( ruleSet.containsKey( crule )) throw new Exception("Unknown source rule: " + crule);
			
			// Add chain rule.
			 ruleSet.get( src ).addRule( ruleSet.remove(crule), Predicate.valueOf( e.getAttribute("operator")) );	
		}
	}
	

	public IRule createRule() {	
		return new Rule();
	}

	
	public IState createState() {
		return new State();
	}

	/**
	 * To do as how do we specify user com.lambda.fsm.tasks
	 */
	public ITask createTask() {
		return null;
	}

	public ITransition createTransition() {
		return new Transition();
	}	
}
