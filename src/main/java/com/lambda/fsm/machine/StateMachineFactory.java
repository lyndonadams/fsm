package com.lambda.fsm.machine;

import java.util.logging.Logger;
import org.w3c.dom.Document;

/**
 * This factory class creates concrete com.lambda.fsm.state machines.
 *  
 * @author Lyndon Adams
 * @version 1.0
 * @since	1.0
 */
public class StateMachineFactory {

	/*
	 * Logfile setting
	 */
	private static Logger logger = Logger.getLogger( StateMachineFactory.class.getName() );
	
	/**
	 * Call this method to create an empty com.lambda.fsm.state com.lambda.fsm.machine ready for user definition.
	 * 
	 * @return the empty com.lambda.fsm.state com.lambda.fsm.machine.
	 */
	public static IMachine createStateMachine(){
		return new StateMachine();
	}
	
	
	/**
	 * Call this method to create a com.lambda.fsm.state com.lambda.fsm.machine from a xml definition.
	 * 
	 * @param fsm com.lambda.fsm.state com.lambda.fsm.machine XML document.
	 * @return the create com.lambda.fsm.state com.lambda.fsm.machine.
	 * @throws Exception is thrown if any issue is encountered during the creation process.
	 */
	public static IMachine createStateMachine(Document fsm) throws Exception{
		IMachine sm= new StateMachine();
		sm.setupMachine( fsm );
		return sm;
	}
	
}
