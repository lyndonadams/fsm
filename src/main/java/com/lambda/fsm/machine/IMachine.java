package com.lambda.fsm.machine;

import org.w3c.dom.Document;
import com.lambda.fsm.rules.IRule;
import com.lambda.fsm.state.IState;
import com.lambda.fsm.tasks.ITask;
import com.lambda.fsm.transition.ITransition;

/**
 * This IMachine interface is used to create finite com.lambda.fsm.state machines from a XML definition file.
 * 
 * @author Lyndon Adams
 * @version 1.0
 * @since	1.0
 */
public interface IMachine {
	
	/**
	 * This method is used to start the com.lambda.fsm.state com.lambda.fsm.machine running.
	 */
	void start();
	
	/**
	 * Call this method with a valid com.lambda.fsm.state com.lambda.fsm.machine XML document to generate the internal representation.
	 * 
	 * @param d the xml file defining the com.lambda.fsm.state com.lambda.fsm.machine.
	 */
	void setupMachine(Document d) throws Exception;
	
	/**
	 * Call this method to find out what states are currently executing.
	 *  
	 * @return array of states.
	 */
	IState[] getExecutingStates();
	
	/**
	 * Call this method to create a com.lambda.fsm.state.
	 * 
	 * @return the created com.lambda.fsm.state object.
	 */
	IState createState();
	
	/**
	 * Call this method to create a com.lambda.fsm.transition.
	 * 
	 * @return the created com.lambda.fsm.transition object.
	 */
	ITransition createTransition();
	
	/**
	 * Call this method to create a rule.
	 * 
	 * @return the created rule object.
	 */
	IRule createRule();
	
	/**
	 * 
	 * @return the created task object.
	 */
	ITask createTask();
}
