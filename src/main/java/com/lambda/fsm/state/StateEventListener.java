package com.lambda.fsm.state;

/**
 * 
 * @author Lyndon Adams
 *
 */
public interface StateEventListener {
	void stateFailed(StateEvent e);
	void stateSuccessful(StateEvent e);
}
