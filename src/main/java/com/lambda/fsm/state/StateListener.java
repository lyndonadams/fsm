package com.lambda.fsm.state;

/**
 * 
 * @author Lyndon Adams
 *
 */
public interface StateListener {
	void performNextState(IState s);
}
