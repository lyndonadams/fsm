package com.lambda.fsm.state;

import com.lambda.fsm.tasks.ITask;
import com.lambda.fsm.transition.ITransition;

/**
 * 
 * @author Lyndon Adams
 *
 */
public interface IState extends Runnable  {
	
	// Transitions
	void addTransitionMap(ITransition t, IState s);
	void removeTransitionMap(ITransition t);

	
	boolean removeTaskListener(ITask e);	
	void addStateChangedListener(StateEventListener l);
	void addIncomingBarrierListener(final ITransition t);
	
	int getNumberOfransitions();
	StateStatus getStatus();
	boolean isFinalState();	
}
