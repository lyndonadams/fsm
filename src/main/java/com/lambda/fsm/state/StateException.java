package com.lambda.fsm.state;

import com.lambda.fsm.transition.TransitionEvent;

/**
 * 
 * @author Lyndon Adams
 *
 */
public class StateException extends Exception {
	String errMsg;
	TransitionEvent te;
	
	public StateException(String e){
		errMsg = e;
	}
	
	public StateException(String e, TransitionEvent tevt){
		this(e);
		te = tevt;
		
	}
	
	public TransitionEvent getTransitionEvent(){
		return te;
	}
	
	public String toString(){
		return errMsg;
	}
}
