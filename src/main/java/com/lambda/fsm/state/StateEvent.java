package com.lambda.fsm.state;

import com.lambda.fsm.transition.TransitionEvent;


/**
 * 
 * @author Lyndon Adams
 *
 */
public interface StateEvent {
	public IState getState();
	public StateException getStateException();
	public TransitionEvent getTransitionEvent();
}
