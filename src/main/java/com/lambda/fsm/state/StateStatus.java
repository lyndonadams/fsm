package com.lambda.fsm.state;

/**
 * 
 * @author Lyndon Adams
 *
 */
public enum StateStatus {
	 WAITING, RUNNABLE, RUNNING, COMPLETED, FAILED;
}
