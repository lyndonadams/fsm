package com.lambda.fsm.state;

import com.lambda.fsm.transition.TransitionEvent;



/**
 * 
 * @author Lyndon Adams
 *
 */
public class StateEventImpl implements StateEvent {

	IState state;
	TransitionEvent t;
	StateException ex;
	
	public StateEventImpl(IState s){
		state = s;
	}
	
	public StateEventImpl(IState s, TransitionEvent te){
		state = s;
		t = te;
	}

	public StateEventImpl(IState s, StateException e){
		this( s );
		ex = e;
	}
	
	public StateEventImpl(IState s,  TransitionEvent tr, StateException e){
		this(s, tr);
		ex = e;
	}
	
	public StateException getStateException() {
		return ex;
	}

	public IState getState() {
		return state;
	}

	public TransitionEvent getTransitionEvent() {
		return t;
	}

}
