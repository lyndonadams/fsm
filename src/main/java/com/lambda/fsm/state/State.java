package com.lambda.fsm.state;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.lambda.fsm.barrier.Barrier;
import com.lambda.fsm.barrier.BarrierEvent;
import com.lambda.fsm.barrier.StateBarrier;
import com.lambda.fsm.barrier.BarrierEventListener;
import com.lambda.fsm.executionpool.ExecutionThreadPool;
import com.lambda.fsm.rules.IRule;
import com.lambda.fsm.tasks.ITask;
import com.lambda.fsm.transition.ITransition;
import com.lambda.fsm.transition.TransitionEvent;
import com.lambda.fsm.transition.TransitionEventListener;


/**
 * 
 * @author Lyndon Adams
 *
 */
public class State implements IState   {

	/*
	 * Logfile setting
	 */
	private Logger logger = Logger.getLogger( State.class.getName() );
	
	// State name
	private String name;
	private int seqNum = -1;
	
	// Final reference to itself
	final private IState me;
	
	
	//	 List of events to process
	private List<ITask> tasks =  Collections.synchronizedList( new ArrayList<ITask>());
	
	// List of transitions with assoicated states	
	private HashMap<ITransition,IState> transitionMap = new  HashMap<ITransition,IState>();
	
	// List of failed com.lambda.fsm.tasks for audit
	private List<ITask> failedtasks = Collections.synchronizedList(  new ArrayList<ITask>());
	
	// Listeners on com.lambda.fsm.state
	private List<StateEventListener> listeners = Collections.synchronizedList( new  ArrayList<StateEventListener>());
	
	// State status
	AtomicReference<StateStatus> status = new AtomicReference<StateStatus>(StateStatus.WAITING);
		
	// Incoming com.lambda.fsm.barrier for transitions
	private Barrier incomingBarrier;
	
	
	// Threading variables
	private Lock statelock = new ReentrantLock();
	private Condition barrierCondition = statelock.newCondition();
	private AtomicInteger  successfullTransitions = new AtomicInteger(0);
	private AtomicInteger failedTransitions = new AtomicInteger(0);
	private AtomicBoolean canProceed = new AtomicBoolean( false);

	private List<IRule> failedrules = new ArrayList<IRule>();
	
	
	/**
	 * Default constructor
	 *
	 */
	public State(){
		me = this;
	}
	
	public State(String s){
		this();
		name = s;
	}
	
	public State(String s, int seq, boolean c){
		this(s);
		seqNum = seq;
		canProceed.set( c);
	}
	
	/**
	 * Set com.lambda.fsm.tasks to run com.lambda.fsm.state
	 */
	public void run()   {		
		if( !isFinalState() ) {
			logger.log( Level.INFO, System.currentTimeMillis() + " "+this.name + " is now running. ");
			status.set( StateStatus.RUNNING );
			for(ITransition t: transitionMap.keySet()) 
				if( t.getRule() != null ) t.getRule().startTasks();		
		} 
	}


	/*
	 * Startup com.lambda.fsm.state in safe com.lambda.fsm.state
	 */
	private void startUpState(IState s){
		if( s.getStatus().compareTo( StateStatus.WAITING ) == 0 ) { 
			status.set( StateStatus.RUNNABLE );
			ExecutionThreadPool.getInstance().execute( s );
		}	
	}
	
	/**
	 * Fire internal event service.
	 * @param s
	 */
	private void fireStateEvent(final BarrierEvent s ){
		if( !isFinalState() ){
				// If com.lambda.fsm.barrier has passed 
				if ( s.hasPassed()) {
					for( StateEventListener l:  listeners){
						l.stateSuccessful( new StateEventImpl( this ) );
					}	
					startUpState( me );	
				}
				// Barrier has failed.
				else {
					logger.log(Level.SEVERE,"State " + name + " has failed must not fire outgoing transitions");
					for(StateEventListener l:  listeners){
						l.stateFailed( new StateEventImpl( this ) );
					}
				}
			} else {
				// Final com.lambda.fsm.state tell all.
				status.set( StateStatus.COMPLETED );
				for(StateEventListener l:  listeners){
					l.stateSuccessful( new StateEventImpl( this ) );
				}
			}
	}
	
	/**
	 * Add State com.lambda.fsm.transition and listener. 
	 */
	public void  addTransitionMap(final ITransition t, final IState s) {
		t.addTransitionListener(new TransitionEventListener(){
			public void transitionRulesFailed(final TransitionEvent e) {
				failedrules.add( e.getTransition().getRule() );
				failedTransitions.incrementAndGet();
			}

			public void transitionRulesSuccessful(final TransitionEvent e) {
				successfullTransitions.incrementAndGet();
			}
		});
		transitionMap.put(t, s);
	}
		

	/**
	 * State guard method to ensure com.lambda.fsm.state com.lambda.fsm.tasks are not kicked off until the com.lambda.fsm.barrier requirement is met.
	 */
	public void addIncomingBarrierListener(final ITransition t){

		if( incomingBarrier == null ){ 
			incomingBarrier =  new StateBarrier( this );
			incomingBarrier.addBarrierListener( new BarrierEventListener(){

				public void barrierFailed(BarrierEvent e) {
					logger.log( Level.SEVERE,"############ BARRIER FAILED FOR STATE " + name + " ################");
					fireStateEvent( e );
				}
	
				public void barrierPassed(BarrierEvent e) {
					logger.log( Level.INFO, System.currentTimeMillis() + " " + " ############ BARRIER PASSED FOR STATE " + name + " ################");
					fireStateEvent( e );
				}			
			});
		}
	 ((StateBarrier)incomingBarrier).addTransitionListener(t);
	}
	
	/**
	 * Remove task listener
	 * @param Task
	 */
	public boolean removeTaskListener(ITask e) {
		return tasks.remove(e);	
	}
	
	/**
	 * Determine if com.lambda.fsm.state is final com.lambda.fsm.state in com.lambda.fsm.state com.lambda.fsm.machine.
	 */
	public boolean isFinalState() {
		return transitionMap.isEmpty();
	}
	
	/**
	 * Add com.lambda.fsm.state change listener.
	 */
	public void addStateChangedListener(StateEventListener l) {
		listeners.add(l);
	}
	
	
	/**
	 * Get the number of failed com.lambda.fsm.tasks for this com.lambda.fsm.state.
	 * @return int
	 */
	public int failedTasks(){
		return failedtasks.size();
	}
	
	/**
	 * Method to return status of com.lambda.fsm.state.
	 */
	public  StateStatus getStatus() {
		return status.get();
	}
	
	
	/**
	 * Add com.lambda.fsm.state task listener.
	 * @param e
	 * @return
	 */
	public boolean addTaskListener(ITask e) {	
		return tasks.add(e);
	}

	
	public void removeTransitionMap(ITransition t) {
		 transitionMap.remove( t);
	}

	@Override
	public int getNumberOfransitions() {
		return transitionMap.size();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final State other = (State) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	public String toString(){
		return name;
	}

}