package com.lambda.fsm.rules;

import com.lambda.fsm.tasks.ITask;

/**
 * 
 * @author Lyndon Adams
 *
 */
public interface IRule {
	
	void addCondition(ITask t1, ITask t2, Predicate p );
	
	void addRule(IRule r1, Predicate p );
	
	void addRuleListener(RuleEventListener o);
	
	boolean hasPassed();

	RuleStatus getStatus();
	
	void startTasks();

}
