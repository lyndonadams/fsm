package com.lambda.fsm.rules;

import com.lambda.fsm.tasks.TaskEvent;

/**
 * 
 * @author Lyndon Adams
 *
 */
public class RuleEventImpl implements RuleEvent {

	TaskEvent task;
	IRule rule;
	RuleException ex;
	
	public RuleEventImpl(IRule r){
		rule = r;
	}
	
	public RuleEventImpl(IRule r, TaskEvent t, RuleException e){
		this(r);
		task = t;
		ex = e;			
	}
	
	public TaskEvent getTaskEvent() {
		return task;
	}

	public RuleException getRuleException() {
		return ex;
	}

	public RuleStatus getStatus() {
		return rule.getStatus();
	}

	public IRule getRule() {
		return rule;
	}

}
