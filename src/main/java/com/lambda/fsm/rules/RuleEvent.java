package com.lambda.fsm.rules;

import com.lambda.fsm.tasks.TaskEvent;

/**
 * 
 * @author Lyndon Adams
 *
 */
public interface RuleEvent {
	public TaskEvent getTaskEvent();
	public RuleException getRuleException();
	public RuleStatus getStatus();
	public IRule getRule();	
}
