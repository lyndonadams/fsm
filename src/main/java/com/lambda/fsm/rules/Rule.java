package com.lambda.fsm.rules;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.lambda.fsm.executionpool.ExecutionThreadPool;
import com.lambda.fsm.tasks.ITask;
import com.lambda.fsm.tasks.TaskEvent;
import com.lambda.fsm.tasks.TaskEventListener;
import com.lambda.fsm.tasks.TaskStatus;

/**
 * @author Lyndon Adams
 *
 */
public class Rule implements IRule{

	/*
	 * Logfile setting
	 */
	private Logger logger = Logger.getLogger( Rule.class.getName() );
	
	protected String name;
	protected IRule nextRule;
	
	// Tasks and predicate
	ITask a, b;
	protected Predicate rulePredicate, taskPredicate;
	
	protected ArrayList<RuleEventListener> observers = new ArrayList<RuleEventListener>();
	
	AtomicReference<RuleStatus> status = new AtomicReference<RuleStatus>(RuleStatus.UNKNOWN);
	AtomicInteger rulefire = new AtomicInteger(0);
	
	// Threading variables
	private Lock rulelock = new ReentrantLock();
	private Condition ruleCondition = rulelock.newCondition();
	
	public Rule(){}
	
	public Rule(String n){
		name = n;
	}
	
	
	public Rule(String name,ITask t1, ITask t2, Predicate p) {
		this(name);
		addTaskListener(this, t1);
		addTaskListener( this, t2);
		addCondition( t1, t2, p);
	}
	
	/**
	 * Start up rule com.lambda.fsm.tasks.
	 */
	public void startTasks() {
		try {
			rulelock.lock();
			if( a.getStatus().compareTo( TaskStatus.WAITING ) == 0  ) ExecutionThreadPool.getInstance().execute( a);
			if( b.getStatus().compareTo( TaskStatus.WAITING ) == 0 ) ExecutionThreadPool.getInstance().execute( b);
			if(nextRule!= null  ) {
				nextRule.startTasks();
			}
		} finally{
			rulelock.unlock();
		}
	}

	/**
	 * Fire task events to listeners.
	 * @param e
	 */
	protected void fireRuleEvent(RuleEvent e){
		for(RuleEventListener l: observers ){			
			switch(  e.getStatus() ){
				case SUCCESS :	logger.log(Level.INFO,"Rule: " + name + " PASSED");								
								l.rulePassed(e); 
								break;
				case FAILED : logger.log(Level.SEVERE,"Rule: " + name + " FAILED");
							  l.ruleFailed(e);
							  break;
				case UNKNOWN : 
				default  :	break;
			}
		}
	}
	
	/**
	 * Setup task listeners.
	 * @param t
	 */
	private void addTaskListener(final IRule r, ITask t){
		t.addTaskListener( new TaskEventListener(){
	
			private void evaluate(TaskEvent taskevent ){
				try {
					rulelock.lock();
					if( status.get().equals( RuleStatus.UNKNOWN ) 
						&& (a.getStatus().compareTo(TaskStatus.COMPLETED) == 0 || a.getStatus().compareTo(TaskStatus.FAILED) == 0)
						&& (b.getStatus().compareTo( TaskStatus.COMPLETED) == 0 || b.getStatus().compareTo( TaskStatus.FAILED) == 0)){
	
						if( hasPassed() == true ) {
							status.set( RuleStatus.SUCCESS );
							fireRuleEvent( new RuleEventImpl( r ));
						}
						else {
							status.set( RuleStatus.FAILED );
							fireRuleEvent( new RuleEventImpl( r, taskevent,  new RuleException("Failed Rule") ));
						}
					}
				}finally{
					rulelock.unlock();
				}
			}
			
			public void taskCompletedSuccessfully(TaskEvent event) {		
				evaluate(event);
			}

			public void taskErrorOccurred(TaskEvent event) {
				evaluate(event);
			}

			// Ignore
			public void taskRunning(TaskEvent event) {
				logger.log(Level.INFO, "Task running: " + event.getTask() );
			}		
		});			
	}
		
	
	/**
	 * Add task2 condition task2.
	 */
	public void addCondition(ITask t1, ITask t2, Predicate p1) {
		a = t1; b = t2;
		taskPredicate = p1;	
	}
	
	/**
	 * Add composite rule with operator.
	 */
	public void addRule(IRule r1, Predicate p) {
		nextRule = r1;
		rulePredicate = p;
	}
	
	/**
	 * 
	 * @param a
	 * @return
	 */
	private boolean evalChainRule(boolean a, Rule n){
		return rulePredicate.evaluate( a,  nextRule.hasPassed( ), rulePredicate);
	}
	
	private boolean evalSimpleRule(){
		
		boolean ret = false;
		try {
			rulelock.lock();
			
			// Evaluate the predicate result
			if(a != null && b!=null) ret = taskPredicate.evaluate(a.isSuccess(), b.isSuccess(), taskPredicate);
			else if( a == null ) ret = b.isSuccess();
			else if( b == null)  ret = a.isSuccess();
			
			status.set(  ( ret == true ) ? RuleStatus.SUCCESS :  RuleStatus.FAILED);
			
		}finally {
			rulelock.unlock();
		}
		return ret;
	}
	
	
	/**
	 * Test if rule has passed.
	 */
	public boolean hasPassed() {
		
		boolean ret = evalSimpleRule();
		
		// Base case with no chain rule
		if( (a != null ||  b!=null) && nextRule== null ) return ret;
				
		// If there is a chain rule call recursive method and pass result
		return  evalChainRule( ret, (Rule)nextRule );
	}

	/**
	 * Get status of rule.
	 */
	public  RuleStatus getStatus() {
		return status.get();
	}

	/**
	 * Add rule event listener.
	 * @param 
	 */
	public void addRuleListener(RuleEventListener o){
		observers.add( o);
	}
	
	/**
	 * Get array of task listeners.
	 */
	public RuleEventListener[] getRuleListeners(){
		return (RuleEventListener[])observers.toArray();
	}

	public String toString(){
		return new String( "Rule name is: " + name );
	}
}