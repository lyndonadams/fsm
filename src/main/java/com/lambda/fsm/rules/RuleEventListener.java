package com.lambda.fsm.rules;

/**
 * 
 * @author Lyndon Adams
 *
 */
public interface RuleEventListener {
	void rulePassed(RuleEvent e);
	void ruleFailed(RuleEvent e);
}
