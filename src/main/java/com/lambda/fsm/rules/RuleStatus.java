package com.lambda.fsm.rules;

/**
 * 
 * @author Lyndon Adams
 *
 */
public enum RuleStatus {
	SUCCESS, FAILED, UNKNOWN;
}
