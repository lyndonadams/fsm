package com.lambda.fsm.rules;

/**
 * 
 * @author Lyndon Adams
 *
 */
public class RuleException extends Exception {
	
	String errMsg;
	
	public RuleException(String e){
		errMsg = e;
	}
	

	public String toString(){
		return errMsg;
	}

}
