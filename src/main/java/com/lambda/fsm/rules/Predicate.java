package com.lambda.fsm.rules;

/**
 * 
 * @author Lyndon Adams
 *
 */
public enum Predicate {
	AND, OR, NOT, XOR;
	
	
	public boolean evaluate(boolean a, boolean b, Predicate p){
		boolean result = false;
	
		switch(p){
		case	AND : result = a&b; break;
		case	OR : result = a|b; break;
		case	XOR : result = a^b; break;
		}
		return result;
	}
	
	
	 public Predicate predicateValue(String s) throws  Exception {
		 Predicate p = null;
		 if( s.compareToIgnoreCase("AND") == 0 ) p = AND;
		 else if( s.compareToIgnoreCase("OR") == 0 ) p = OR;
		 else if( s.compareToIgnoreCase("NOT") == 0 ) p = NOT;
		 else if( s.compareToIgnoreCase("XOR") == 0 ) p = XOR;
		 else throw new Exception("Unknown predicate: " + s);
		 
		return p;
	}
	
}
