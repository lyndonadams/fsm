package com.lambda.fsm.executionpool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * This class provides a central threading control for the FSM. The ExecutionThreadPool takes runnable com.lambda.fsm.tasks then 
 * adds them in to the execution pool ready for execution when free threads become available.
 * 
 * @author Lyndon Adams
 * @version 1.0
 * @since	1.0
 */
public class ExecutionThreadPool {

	private static ExecutionThreadPool instance;
	
	private int poolSize = 10;
	private int maxPoolSize = 30;
	private long keepAliveTime =  60L * 5;
 
 
    // LinkedBlockingQueue
    final private ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(10);
    
	
	// Thread pool size etc
	private static ThreadPoolExecutor  pool;
	
	private ExecutionThreadPool(){
		pool = new ThreadPoolExecutor(poolSize, maxPoolSize,  keepAliveTime, TimeUnit.SECONDS, queue);
	}
	
	/**
	 * Get an instance of the thread pool.
	 * 
	 * @return An instance of the ExecutionThreadPool class.
	 */
	public synchronized static ExecutionThreadPool getInstance(){
		if(instance == null) instance =  new ExecutionThreadPool();
		return instance;
	}
	
	/**
	 * Add a {@link Runnable} task to thread pool for execution.
	 * 
	 * @param r the task to thread for later execution.
	 */
	public void execute( Runnable r){
		//pool.execute(r);
		pool.submit( r );
	}
	
	/**
	 * Get access to the underlying thread pool.
	 * 
	 * @return the thread pool.
	 */
	public ThreadPoolExecutor getPool(){
		return pool;
	}
	
}
