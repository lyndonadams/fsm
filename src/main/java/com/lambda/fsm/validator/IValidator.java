package com.lambda.fsm.validator;

import com.lambda.fsm.machine.IMachine;

/**
 * 
 * @author Lyndon Adams
 *
 */
public interface IValidator {
	boolean isValid(IMachine m);
}
