# Filename: Dockerfile
FROM openjdk:13-alpine
WORKDIR /app/fsm
COPY ./* /app/fsm/
CMD [ "/bin/sh", "-c", "/app/fsm/bin/startJoule.sh -s ${SOURCEFILE} -e ${ENGINEFILE} -p ${PUBLISHFILE} -m 1G"]

